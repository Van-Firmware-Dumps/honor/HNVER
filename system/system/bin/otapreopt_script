#!/system/bin/sh

#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This script will run as a postinstall step to drive otapreopt.

TARGET_SLOT="$1"
STATUS_FD="$2"

# Maximum number of packages/steps.
MAXIMUM_PACKAGES=1000

# Reporting time interval.
REPORT_INTERVAL=5

# pre-reboot job status: [0-running],[1-pause]
JOB_STATUS=0

# First ensure the system is booted. This is to work around issues when cmd would
# infinitely loop trying to get a service manager (which will never come up in that
# mode). b/30797145
BOOT_PROPERTY_NAME="dev.bootcomplete"

# If system support pre-reboot job.
PREREBOOT_PROP="dalvik.vm.enable_pr_dexopt"

BOOT_COMPLETE=$(getprop $BOOT_PROPERTY_NAME)
if [ "$BOOT_COMPLETE" != "1" ] ; then
  echo "Error: boot-complete not detected."
  # We must return 0 to not block sideload.
  exit 0
fi

# Compute target slot suffix.
# TODO: Once bootctl is not restricted, we should query from there. Or get this from
#       update_engine as a parameter.
if [ "$TARGET_SLOT" = "0" ] ; then
  TARGET_SLOT_SUFFIX="_a"
elif [ "$TARGET_SLOT" = "1" ] ; then
  TARGET_SLOT_SUFFIX="_b"
else
  echo "Unknown target slot $TARGET_SLOT"
  exit 1
fi

# Get time limit
TIME_LIMIT=$((5 * 60))
DEXOPT_CONFIG_PROPERTY_NAME="msc.art.dexopt.config"
DEXOPT_CONFIG=$(getprop $DEXOPT_CONFIG_PROPERTY_NAME)
length=${#DEXOPT_CONFIG}
if [ $length -gt 0 ]; then
    first_char=$(echo $DEXOPT_CONFIG | cut -c 1)
    number=$(expr $first_char + 0)
    if [ $number -ge 0 ] || [ $number -le 10 ]; then
      TIME_LIMIT=$(($number*60))
    fi
    echo "get time limit: $TIME_LIMIT"
fi

update_progress() {
  local progress_time="$1"
  local progress=$(echo "scale=2; $progress_time/$TIME_LIMIT*0.99" | bc)
  local formatted_progress=$(printf "%.2f" "$progress")
  print -u${STATUS_FD} "global_progress $formatted_progress"
}

print_progress_prereboot() {
  i=0
  while ((i<$TIME_LIMIT)) ; do
    update_progress $i &
    sleep $REPORT_INTERVAL
    i=$((i+$REPORT_INTERVAL))
  done

  print -u${STATUS_FD} "global_progress 1.0"
  pm art pr-dexopt-job --timeout

  exit 0
}

PREREBOOT_VALUE=$(getprop $PREREBOOT_PROP)
if [ "$PREREBOOT_VALUE" == "true" ] ; then
  print_progress_prereboot &
  echo "PREREBOOT_VALUE: $PREREBOOT_VALUE"
  pm art pr-dexopt-job --schedule --slot "$TARGET_SLOT_SUFFIX"
  pm art pr-dexopt-job --start
  wait
else
  print -u${STATUS_FD} "global_progress 1.0"
fi

exit 0
